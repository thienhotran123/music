import React, { useEffect, useRef, useState } from "react";
import styles from "./LoginPageCss.module.scss";
import Hderder from "../ContainerComponent/Header/Header";
import Footer from "../ContainerComponent/Footer/Footer";
const HomePage: React.FC = () => {
  const WidthRef = useRef<HTMLDivElement>(null);
  const [DataValue, setDataValue] = useState([]);
  useEffect(() => {
    const myArray: number[] = [];
    const numberWidthSpan: number = Math.floor(
      WidthRef.current.offsetWidth / 36
    );
    for (let i = 0; i < numberWidthSpan; i++) {
      myArray.push(i);
    }
    setDataValue(myArray);
  }, []);
  return (
    <div className={styles.body}>
      <div ref={WidthRef} className={styles.HomeContainer}>
        <div className={styles.bubbles}>
          {DataValue.map((item, index) => (
            <span
              key={index}
              style={{ "--random-number": Math.random() * 40 + 3,} as React.CSSProperties
              }
            ></span>
          ))}
        </div>
        <form className={styles.FormLogin}>
          <div className={styles.FormLogin_Group}>
            <div className={styles.FormLogin_input}>
              <input type="text" className={styles.FormLogin_inputNameForm} />
              <label htmlFor="">Email</label>
            </div>
            <div className={styles.FormLogin_input}>
              <input type="text" className={styles.FormLogin_inputPassForm} />
              <label htmlFor="">PassWord</label>
            </div>
            <div className={styles.FormLoginBtn}>
              <button className={styles.FormLoginBtnLog}>
                Login
              </button>
              <button className={styles.FormLoginBtnSig}>
                Sign Up
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default HomePage;
