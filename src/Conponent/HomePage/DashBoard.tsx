import React from "react";
import styles from "./DashBoardCss.module.scss";
const HomePage: React.FC = () => {
  return (
    <div className={styles.DashBoardContainer}>
      <div className={styles.DashBoardContainer_Header}>
        <h2 className={styles.DashBoardContainer_Header_Content}>
          MucSic Player
        </h2>
        <div className={styles.DashBoardContainer_Header_LinkSearch}>
        <div className={styles.DashBoardContainer_Header_links}>
          <a href="">Home</a>
          <a href="">About</a>
          <a href="">Gaming</a>
          <a href="">Services</a>
        </div>
        <div className={styles.DashBoardContainer_Header_Search}>
          <input type="text" />
        </div>
        </div>
      </div>
      <div className={styles.DashBoardContainer}></div>
      <div className={styles.DashBoardContainer}></div>
    </div>
  );
};
export default HomePage;
