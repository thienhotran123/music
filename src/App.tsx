import React from 'react'
import {Routes , Route} from 'react-router-dom'
import LoginPage from './Conponent/LoginPage/LoginPage'
import DashBoard from './Conponent/HomePage/DashBoard'
const App = () => {
  return (
    <Routes>
      <Route path='/' Component={DashBoard}/>
      <Route path='/home' Component={LoginPage}/>
    </Routes>
  )
}

export default App