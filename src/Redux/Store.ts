import { configureStore } from "@reduxjs/toolkit";
import { type } from "os";
import  StateSlice  from "./Slice";

export const store=configureStore({
    reducer:{
        StateData :StateSlice
    }
})

export type RootState  =ReturnType<typeof store.getState>

export  type AppDisPatch=typeof store.dispatch