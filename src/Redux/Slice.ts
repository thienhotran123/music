import { createSlice } from "@reduxjs/toolkit";
import  type {PayloadAction} from '@reduxjs/toolkit'
export interface DataState {
    value :number
}

const initialState : DataState={
    value :0
}
export const StateSlice=createSlice({
    name :"Slice",
    initialState,
    reducers:{

    }
})

export const {} = StateSlice.actions
export default StateSlice.reducer